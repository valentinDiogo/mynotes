# Gestion de projet avec Git

- [les branches](#les-branches)
  - [creer une branche](#creer-une-branche-)
- [récuperer et envoyer son travail](#récuperer-et-envoyer-son-travail)
  - [commit, push & merge](#commit-push-merge)
  - [récupération du travail chez le chef de projet](#récupération-du-travail-chez-le-chef-de-projet)
  - [récéption de l'information chez le developpeur](#récéption-de-linformation-chez-le-developpeur)


## Les branches

La branche principale est appelée Master, nous devons travailler sur notre propre branche et on mergera ensuite le travail sur la master.

#### Creer une branche :

1. Pour que nos modification soient intégrées au master, il faut cliquer en bas sur Git: master
2. Créer une nouvelle branche
3. On peut alterner entre les deux versions avec checkout
4. Il faut commiter avant de checkouter la branche
5. Une fois la branche crée, il faut demander l'autorisation pour l'envoyer vers la master
6. Il faut aller sur git et cliquer sur create merge request
7. Verifier si on va de branch vers master
8. On peut s'assigner pour faire la demande au chef
9. Enfin pour recuperer la modification on retourne sur webstorm, puis sur la master, on clique sur update (fleche bleue) et on clique sur merge.

## Récuperer et envoyer son travail

#### Commit, push & merge

1. Cliquer sur la fleche bleue pour récuperer la master
2. Créer une branche pour apporter des modifications au projet
3. Faire les modifications
4. Commiter et pusher
5. Aller sur gitlab et cliquer sur create merge request
6. Choisir le responsable ou chef de projet
7. Cliquer sur submit, le projet a été envoyé

#### Récupération du travail chez le chef de projet

1. Reception d'une notification qui prévient le chef de l'arrivé de la request
2. Apercu des conflits du code en cliquant sur changes et validation si tout va bien en cliquant sur merge
3. En cas de conflit on clique sur resolve conflict
4. Le chef écrit au developpeur et demande de se mettre à jour et de résoudre les conflits

#### Récéption de l'information chez le developpeur

1. Se mettre sur master et clicker sur la fleche bleue pour recuperer la dernière version
2. Retourner sur sa branche, cliquer sur vcs git et cliquer sur merge changes
3. Cliquer sur master et la merger dans la branche
4. Resoudre les conflits en cliquant sur le fichier puis sur merge un par un
6. Modifier le code et cliquer sur apply
7. Signaler que la merge a été faite en appuyant sur shift + commande + k (racourci push)

 - [Comment configurer Git ?](Configurer Git.md)