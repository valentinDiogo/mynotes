# Configurer Git

 - [Comment travailler avec Git](Travailler avec Git.md)

## Les étapes
1. Ouvrir le terminal de commande
2. Saisir son username:
**git config** ` --global user.name "FIRST_NAME LAST_NAME" `
3. Saisir son adresse email:
**git config** ` --global user.email "MY_NAME@example.com" `
4. Dans settings, cliquer sur plugin et installer gitlab projects
5. Redémarrer Webstorm
6. Rertourner dans settings, puis version control et aller sur gitlab
7. Saisir https://gitlab.com dans la 1ere ligne
8. Saisir gitlab.com dans la 2eme
9. Cliquer sur token, puis sur API et créer le token
10. Saisir le token
11. Git est activé, saisir git init dans le terminal de commande pour s'en assurer

# Le fichier .gitignore

Pour eviter d'envoyer certains éléments inutiles sur git (nodemodule, package.json etc).
Il faut faire un gitignore au tout début, lors de la création du projet.
Si on rajoute un fichier avant le git ignore il ne sera pas ignoré.

## Les étapes

1. Créer un fichier dans la racine appelé .gitignore puis l'ajouter a git
2. Dans ce fichier, mettre tous les fichiers et dossiers que l'on ne veut pas en commencant par un .
3. Pour retirer un fichier (idea par exemple) qui n'a pas été ignoré, Saisir git rm -rf .idea
4. On refait commit, idea aura disparu
