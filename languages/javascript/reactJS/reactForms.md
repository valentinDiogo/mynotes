# Les Formulaires

Les formulaires HTML fonctionnent un peu différemment des autres éléments du DOM en React car ils possèdent naturellement un état interne.


````html
<form onSubmit={handleSubmit}>
    <label htmlFor="name">Nom</label>
    <input
        value={state.name}
        onChange={handleInput}
        type="text" id="name"/>
    <label htmlFor="age">Age</label>
    <input
        value={state.age}
        onChange={handleInput}
        type="number" id="age"/>
    <button type="button">Annuler</button>
    <button type="submit">Valider</button>
</form>
````

Dans cet exemple, << handleSubmit >> est la fonction correspondant a la validation du formulaire. 
Le label et l'input sont reliés grace à un id et a la propriétés << htmlFor >> du label.
React considère le premier boutton comme un boutton de type submit. On précise donc nous-même quel boutton est choisi pour ça grace à la propriété << type >>.