# Les Events


Comme HTML, React peut effectuer des actions en fonction des événements de l'utilisateur. 
Les événements React sont écrits en camelCase. Les gestionnaires d'événements React sont écrits en utilisant des interpolations.

Par exemple l'event ` onClick ` s'écrit sous la forme :

````html
<button onClick={shoot}>Take the Shot!</button>
````

'shoot' étant le nom de la fonction et/ou de la méthode souhaitée.

**Exemple de quelques events sous forme React** : 

` onClick ` : Gerer les clics

` onMouseLeave ` : La souris quitte le champ concerné

` onMouseOver ` : La souris passe sur le champ

` onChange ` : Un input change de valeur

` onSubmit ` : Evenement spécial pour les formulaires