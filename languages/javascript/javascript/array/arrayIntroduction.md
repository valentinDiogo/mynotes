# Introduction à Javascript

## Lier un fichier javascript à notre fichier html

Comme pour le css on relie le fichier dans la partie head de notre page html avec la balise `<script>`.

`<script src="index.js"></script>`

## Afficher un élement dans la console

Pour afficher un élément dans la console on utilise `console.log`.

`console.log('coucou');`

`console.log(23);`

## Les valeurs primitifs

Il s'agit des valeurs principales qu'il est possible d'utiliser avec javascript.

### Strings

Il s'agit de valeurs de type texte. Les strings sont toujours entre guillemet.

En cas de mot avec une apostrophe, et pour ne pas avoir de soucis avec le ' on écrit de cette façon : `'je m/'appelle Valentin'`

### Numbers

Il s'agit de valeurs de type nombre.
`42`

### Booleans

Il s'agit d'une valeur qui est soit vrais soit fausse. 

`true`
`false`

### Undefined

C'est une valeur qui n'existe pas

### Null

C'est une valeur qui est vide

## Les variables

Une variable est comme une boite dans laquelle on va stocker une donée.
Pour déclarer une variable on écrit `let name = 'hamza';` ou `const age = 26;`.

* la variable var n'est plus utilisé
* la variable let peut être modifié
* la variable const ne change plus

## Les fonctions

Une fonction est une boite dans laquelle on va stocker des variables. Ci-dessous un exemple avec des valeurs de retour `return`.

````javascript
const number1 = 5;
const number2 = 3;

function addition (param1, param2) {
  return param1 + param2;
}

function soustraction (param1, param2) {
  return param1 - param2;
}

const result = addition(number1, number2);
const result2 = soustraction(number1, number2);

console.log(result);
console.log(result2);
````

## Les objets

Plutôt que de créer plusieurs variables, on peut les regrouper dans un objet.

````javascript
const me = {
nom : 'Diogo',
prenom: 'Valentin',
age : 25,
};
````

Pour afficher le prénom par exemple on écrit `console.log(me.prenom);`

## Les prédicats

Il est possible de vérifier si une valeur est vrais ou fausse.
````javascript
const isMajor = me.age >= 18;
````
La console devrait renvoyer `true` parce que hamza a plus de 18 ans.

### Composer des prédicats

Il est possible de vérifier si plusieurs conditions soit vrais ou fausses avec les symboles `&&` (pour ET) et `||` (pour OU).

````javascript
const isMajorAndIsName = (me.prenom === 'Valentin') && (me.age >= 18);

console.log(isMajorAndIsName);
````

La console devrait renvoyer `true`.


## La structure conditionnelle if  else

Il est possible de renvoyer un résultat en fonction des conditions que l'on va fixer.

````javascript
const person = {
    name : 'John Doe',
    age : 25,
    gender : 'M',
};

if (person.gender === 'F') {
  console.log("coucou je suis une femme");
} else if (person.gender === 'M') {
  console.log("coucou je suis un homme");
} else {
  console.log("inconnu");
}
````

Il est aussi possible de mettre cette structure conditionnelle dans une fonction.

````javascript
function sayGender(person) {
    if (person.gender === 'F') {
      console.log("coucou je suis une femme");
    } else if (person.gender === 'M') {
      console.log("coucou je suis un homme");
    } else {
      console.log("inconnu");
    }
}

sayGender(person);
````

Résultat attendu : `"coucou je suis un homme"`.
