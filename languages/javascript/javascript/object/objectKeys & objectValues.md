# Object keys et Object values

Ils permettent de transformer des objets en arrays.

## Object.keys

On veut récuperer les clefs de l'objet hostels dans un tableau :

````javascript
const keys = Object.keys(hostels);

console.log(keys);
````

La console va renvoyer `["hotel1", "hotel2", "hotel3"]`

## Object.values

Pour récuperer les values dans un tableau :

````javascript
const values = Object.values(hostels);

console.log(values);
````

La console va renvoyer les values des trois hôtels.

Si on veut transformer les rooms de l'hôtel1 (sachant que les rooms sont sous la forme d'objets) :

````javascript
const rooms = Object.values(hostels.hotel1.rooms);

console.log(rooms);
````

La console va renvoyer sous forme d'un array les rooms de l'hotel1.
